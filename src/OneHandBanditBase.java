import java.util.Objects;
import java.util.Scanner;

public class OneHandBanditBase {
    private final int CHERY = 1;
    private final int APPLE = 3;
    private final int LEMON = 5;
    private final int DIAMOND = 10;
    private final int GOLDCOIN = 100;
    private final int JACKPOT = 1000;
    int balance = 10000;
    String[] sloth1 = new String[]{"*Chery*", "*Apple*", "*Lemon*", "*Diamond*", "*GoldCoin*", "*777*"};
    String[] sloth2 = sloth1;
    String[] sloth3 = sloth1;
    private int bet;
    private int resultGoal;

    public OneHandBanditBase() {
        System.out.println("ОДНОРУКИЙ БАНДИТ v1.01");
        this.bet = setBet();
        this.resultGoal = gameResult();
    }

    public int setBet() {
        Scanner sc = new Scanner(System.in);
        System.out.println(" Введите ставку: ");
        this.bet = sc.nextInt();
        System.out.println(" Ваша ствака равна: " + bet);
        if (bet > balance) {
            System.out.println("Увас недостаточно средств на балансе");
        }
        return bet;
    }

    public int gameResult() {
        String r1 = sloth1[(int) Math.floor(Math.random() * sloth1.length)];
        String r2 = sloth2[(int) Math.floor(Math.random() * sloth2.length)];
        String r3 = sloth3[(int) Math.floor(Math.random() * sloth3.length)];
            if (Objects.equals(r1, r2) && Objects.equals(r2, r3)) {
                System.out.println(r1 + " | " + r2 + " | " + r3 + "\nПОБЕДА!!!");
                if ("*Chery*".equals(r1)) {
                    balance = balance + bet * CHERY;
                } else if ("*Apple*".equals(r1)) {
                    balance = balance + bet * APPLE;
                } else if ("*Lemon*".equals(r1)) {
                    balance = balance + bet * LEMON;
                } else if ("*Diamond*".equals(r1)) {
                    balance = balance + bet * DIAMOND;
                } else if ("*GoldCoin*".equals(r1)) {
                    balance = balance + bet * GOLDCOIN;
                } else if ("*777*".equals(r1)) {
                    balance = balance + bet * JACKPOT;
                }
            } else {
                System.out.println(r1 + " | " + r2 + " | " + r3 + "\nПРОИГРАЛ :-( ");
                balance = balance - bet;
            }
            return balance;
        }

    public String showInfo() {
        return "Ваш баланс: " + balance + " Ваша ставка: " + bet;
    }
}


